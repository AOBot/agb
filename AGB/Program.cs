﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows;
using AGB.IDemonbuddy;
using AGB.Managerial;
using AGBLoader;
using D3.Pathing;
using Zeta;
using Zeta.Common;
using Zeta.Common.Plugins;
using Zeta.CommonBot;
using Zeta.Internals;
using Zeta.Internals.Actors;
using D3;
using System;
using System.Diagnostics;


internal class _Demonbuddy
{
    public static bool Loaded = false;
    private static AGBSettingsWindow _window = null;

    public static AGBSettingsWindow aWindow
    {
        get
        {
            if (_window == null)
            {
                _window = new AGBSettingsWindow();
            }
            return _window;
        }
    }


    public static void OnLoad()
    {
        if (Loaded) return;

        foreach(var plugin in PluginManager.Plugins)
        {
            if (plugin.Plugin.Name.Contains("AGB.Loader"))
            {
                Loaded = true;
                plugin.Enabled = true;
            }
        }

        
        Logging.Write("Instanciated");
        /* Display window in its own thread */
        /*MainWindow window = new MainWindow();

        Thread loaderThread = new Thread(
            delegate()
                {
                Application.Run(window);
            });

        loaderThread.Start();


        /* Loads modules and starts pulsing thread */

        //aWindow = new AGBSettingsWindow();
        BotManager.Start();
    }

    public static void OnLoadCond()
    {
        if (!AGB.Properties.Settings.Default.LoadOnStartup) return;
        var x = AppDomain.CurrentDomain;
    }

    public static void Unload()
    {
        if (!Loaded) return;
        Loaded = false;
        if (_window != null) _window.Close();
        _window = null;
        BotManager.Stop();
    }

    public static void Debug(String s)
    {
        Zeta.Common.Logging.Write(s);
    }

}

