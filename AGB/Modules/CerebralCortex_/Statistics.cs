﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zeta;
using Zeta.Common;
using Zeta.CommonBot;
using Zeta.TreeSharp;

namespace AGB.Modules.CerebralCortex_
{
    public class SarkothStatistics
    {
        public int numberOfRuns = 0;
        public int StartingGold = 0;
        public DateTime StartingDate = DateTime.Now;
        public int cellarsFound = 0;
        public int deaths = 0;
        public int stucks = 0;
        public long lastRunGoldStart = 0;
        public long lastRunGoldEnd = 0;
        public long lastRunStartTick = 0;
        public long lastRunEndTick = 0;


        public long GoldGained()
        {
            if (ZetaDia.Me == null) return 0;
            if (ZetaDia.Me.Inventory == null) return 0;
            if (StartingGold == 0) StartingGold = ZetaDia.Me.Inventory.Coinage;
            return (ZetaDia.Me.Inventory.Coinage - StartingGold);
        }

        public string TimeSpanToString(TimeSpan t)
        {

            string h = "", m = "", s = "", ms = "";

            if (t.Hours < 10) h = "0" + t.Hours; else h = "" + t.Hours;
            if (t.Minutes < 10) m = "0" + t.Minutes; else m = "" + t.Minutes;
            if (t.Seconds < 10) s = "0" + t.Seconds; else s = "" + t.Seconds;
            int tm = t.Milliseconds/10;
            ms = ""+tm;
            //if (tm < 10) ms = "0" + tm; else ms = "" + tm;
            return h+":"+m+":"+s+":"+ms;
        }

        public TimeSpan Runtime()
        {
            return DateTime.Now.Subtract(StartingDate);
        }

        public TimeSpan LastRunTime()
        {
            TimeSpan retVal = new TimeSpan((lastRunEndTick - lastRunStartTick)*10000L);
            return retVal;
        }

        public long GoldGainedLastRun()
        {
            return (lastRunGoldEnd - lastRunGoldStart);
        }

        public long AverageGoldPerRun()
        {
            double result = GoldGained()/(double)cellarsFound;
            var res = (int) result;
            return res;
        }

        public int GoldPerHour()
        {
            var gg = GoldGained()*60*60;
            var res = gg/DateTime.Now.Subtract(StartingDate).TotalSeconds/1000;
            return (int)res;
        }

        public override String ToString()
        {


            var lrt = TimeSpanToString(LastRunTime());
            var rt = TimeSpanToString(Runtime());

            var gg = GoldGained()/1000;

            return String.Format("Stats:\n\tLast run took {0} | Cellar Found in {1}/{2} Runs over {3} \n\tAverage Gold per Cellar: {4} gold | A total of: {5}k gold since start | Coming to {6}k GP/H\n\tTotal Deaths: {7} | Total Stucks: {8}", lrt, cellarsFound, numberOfRuns, rt, AverageGoldPerRun(), gg, GoldPerHour(), deaths, stucks);

        }
    }
}
