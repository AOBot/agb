﻿using System;
using AGB.Managerial;
using Zeta.TreeSharp;

namespace AGB
{
    public class ModuleException : Exception
    {
        public Module Module;

        public ModuleException(Module module, string message)
            : base(message)
        {
            Module = module;
        }
    }

    public delegate void ModuleExceptionEventArgs(ModuleException e);
    public delegate void ModuleEventArgs(Module module, string message);

    public abstract class Module
    {
        public event ModuleEventArgs StatusUpdated;
        public event ModuleEventArgs Warning;
        public event ModuleExceptionEventArgs ExceptionThrown;

        public string BaseDirectory;

        public string Name;
        public string Author;
        public string Version;

        public GroupComposite Behavior;
        private object Context = new object();

        /// <summary>
        /// Check for required modules here
        /// </summary>
        /// <param name="game"></param>
        public virtual void Load() { }
        public virtual void Unload() { }
        public virtual void Start() { }
        public virtual void LobbyEntered() { }
        public virtual void GameEntered() { }
        public virtual void GameExited() { }
        public virtual void Pulse(PulseArgs args) { }

        protected void RaiseUpdate(string message)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, message);
        }

        protected void RaiseWarning(string message)
        {
            if (Warning != null)
                Warning(this, message);
        }

        protected void ThrowModuleException(ModuleException e)
        {
            // Implement this
            throw new NotImplementedException();
        }
    }
}