﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AGB.Managerial;
using AGB.Modules.FrontalLobe_;
using Zeta;
using Zeta.Common;
using Zeta.Internals.Actors;
using Zeta.Internals.SNO;
using Zeta.TreeSharp;
using Action = Zeta.TreeSharp.Action;

namespace AGB.Modules
{
    public class Face : Module
    {
        public Face()
        {
            Name = "Face";
            Author = "none";
            Version = "0.1.0";
        }

        public bool Interact(CachedUnit target)
        {
            return target.Interact();
        }

        public bool Interact(String target)
        {
            var tar = BotManager.UnitManager.GetObject<CachedUnit>(target);
            if (tar != null) return Interact(tar);
            return false;
        }

        public bool Operate(CachedGizmo target)
        {
            return target.Interact();
        }

        public bool Operate(String target)
        {
            var tar = BotManager.UnitManager.GetObject<CachedGizmo>(target);
            if (tar != null) return Operate(tar);
            return false;
        }

        public bool HasLineOfSight(Vector3 pos)
        {
            return ZetaDia.Physics.Raycast(pos + new Vector3(0f, 0f, 2f), ZetaDia.Me.Position + new Vector3(0f, 0f, 2f), NavCellFlags.AllowWalk);
        }

    }
}
