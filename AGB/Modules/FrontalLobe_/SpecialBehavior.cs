﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zeta.TreeSharp;
using Action = Zeta.TreeSharp.Action;

namespace AGB.Modules.FrontalLobe_
{
    class ActionAlwaysFail : Action
    {
        public ActionAlwaysFail() 
            : base(delegate(object context) { return RunStatus.Failure; })
        {
            
        } 
    }

    class ActionAlwaysSucceed : Action
    {
        public ActionAlwaysSucceed()
            : base(delegate(object context) { return RunStatus.Success; })
        {

        }
    }

}
