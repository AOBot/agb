﻿using System;
using System.Globalization;
using System.Reflection;
using System.Windows;
using AGB.IDemonbuddy;
using AGB.Managerial;
using AGB.Modules.FrontalLobe_;
using D3.Pathing;
using Zeta;
using Zeta.Common;
using Zeta.CommonBot;
using Zeta.CommonBot.Logic;
using Zeta.Internals;
using Zeta.Internals.Actors;
using Zeta.Navigation;
using Zeta.TreeSharp;
using Action = Zeta.TreeSharp.Action;


namespace AGB.Modules
{
    public class FrontalLobe : Module
    {

        private GroupComposite TheBot;
        private CerebralCortex _cortex;

        public FrontalLobe()
        {
            Name = "FrontalLobe";
            Author = "none";
            Version = "0.1.0";
            _cortex = BotManager.CerebralCortex;
        }

        public override void Start()
        {
            Hook();
            if (Zeta.CommonBot.BotMain.IsRunning)
            {
                HandleBotStart(null);
            }
        }

        public override void Unload()
        {
            UnHook();
        }

        public void Hook()
        {
            BotMain.OnStop += HandleBotStop;
            BotMain.OnStart += HandleBotStart;
        }

        public void UnHook()
        {
            BotMain.OnStop -= HandleBotStop;
            BotMain.OnStart -= HandleBotStart;
        }

        public void ResetBot()
        {
            Zeta.CommonBot.Settings.CharacterSettings.Instance.RepairWhenDurabilityBelow = 10;

            TreeHooks.Instance.ReplaceHook("TreeStart", new Sequence());
            TheBot.Stop(null);
            TheBot = null;

            BotManager.UnitManager.Clear();

            Multiverse.Clear();

            var theBot = new Sequence();
            theBot.AddChild(BotManager.BotManagerBehavior);
            theBot.AddChild(_cortex.GenerateSarkothQuestBehavior());
            TheBot = theBot;

            GC.Collect();
            GC.WaitForPendingFinalizers();

            TreeHooks.Instance.ReplaceHook("TreeStart", TheBot);
        }

        private void HandleBotStart(IBot bot)
        {
            TreeHooks.Instance.ReplaceHook("TreeStart", new Sequence());

            Zeta.CommonBot.Settings.CharacterSettings.Instance.RepairWhenDurabilityBelow = 10;

            Multiverse.Clear();
            BotManager.UnitManager.Clear();

            var theBot = new Sequence();
            theBot.AddChild(BotManager.BotManagerBehavior);
            theBot.AddChild(_cortex.GenerateSarkothQuestBehavior());
            TheBot = theBot;
            _cortex.Stats.StartingDate = DateTime.Now;
            if (ZetaDia.Me != null) _cortex.Stats.StartingGold = ZetaDia.Me.Inventory.Coinage;

            TreeHooks.Instance.ReplaceHook("TreeStart", TheBot);

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        private void HandleBotStop(IBot bot)
        {
            TreeHooks.Instance.RemoveHook("TreeStart", TheBot);
        }

        public override void Pulse(PulseArgs args)
        {
            /*if (!TheBot.IsRunning) TheBot.Start(this);
            RunStatus s = TheBot.Tick(this);
            if (s == RunStatus.Success) TheBot.Start(this);*/

        }

    }
}
