﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zeta.Common;
using Zeta.TreeSharp;

namespace AGB.Modules.FrontalLobe_
{
    class TreeCollector
    {
        public static void Collect(GroupComposite tree)
        {
            while (tree.Children.First() != null){
                var child = tree.Children.First();
                if (child is GroupComposite)
                {
                    Collect(child as GroupComposite);
                    Logging.Write("Deleting " + child);
                    tree.Children.Remove(child);
                } else
                {
                    Logging.Write("Deleting "+child);
                    tree.Children.Remove(child);
                }
            }
        }
    }
}
