﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zeta.Common;
using Zeta.TreeSharp;
using Action = System.Action;

namespace AGB.Modules.FrontalLobe_
{
    class Inverter : Decorator
    {
        public Inverter(Composite Child)
            : base(delegate(object context) { return true; }, Child)
        {
            
        }

        public override RunStatus Tick(object context)
        {
            RunStatus status = base.Tick(context);
            if (status == RunStatus.Failure) return RunStatus.Success;
            if (status == RunStatus.Success) return RunStatus.Failure;
            return status;

        }
    }

    class AlwaysTrue : Decorator
    {
        public AlwaysTrue(Composite Child)
            : base(delegate(object context) { return true; }, Child)
        {

        }

        public override RunStatus Tick(object context)
        {
            RunStatus status = base.Tick(context);
            return RunStatus.Success;

        }
    }
    class AlwaysFalse : Decorator
    {
        public AlwaysFalse(Composite Child)
            : base(delegate(object context) { return true; }, Child)
        {

        }

        public override RunStatus Tick(object context)
        {
            RunStatus status = base.Tick(context);
            return RunStatus.Failure;

        }
    }

    class RunAfter : Decorator
    {
        private Action _runAfter;

        public RunAfter(Composite Child, Action runAfter)
            : base(delegate(object context) { return true; }, Child)
        {
            _runAfter = runAfter;
        }

        public override RunStatus Tick(object context)
        {
            RunStatus status = base.Tick(context);
            _runAfter.Invoke();
            return status;

        }
    }


}
