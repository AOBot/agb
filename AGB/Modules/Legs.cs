﻿using System;

using System.Drawing;
using AGB.Managerial;
using D3.Pathing;
using Zeta;
using Zeta.Common;
using Zeta.TreeSharp;
using Action = Zeta.TreeSharp.Action;


namespace AGB
{
    public class Legs : Module
    {
        public Legs()
        {
            Name = "Legs";
            Author = "Baby Superman";
            Version = "0.1.0";
        }

        public double MinDistDefault = 20;

        public override void Load()
        {

        }

        public bool FloatPointComp(float x1, float y1, float x2, float y2)
        {
            return (x1 > x2 - 5 && x1 < x2 + 5 && y1 > y2 - 5 && y1 < y2 + 5);
        }

        public float GetDistance(SpeedyAStarNode node)
        {
            return GetDistance(node.X, node.Y, ZetaDia.Me.Position.X, ZetaDia.Me.Position.Y);
        }

        public float GetDistance(float x, float y)
        {
            return GetDistance(x, y, ZetaDia.Me.Position.X, ZetaDia.Me.Position.Y);
        }

        public float GetDistance(float x, float y, float x2, float y2)
        {
            return (float)Math.Sqrt((x - x2) * (x - x2) + (y - y2) * (y - y2));
        }


        public void StopWalking()
        {
            Vector3 walkPos = ZetaDia.Me.Position;
            ZetaDia.Actors.Me.Movement.MoveActor(walkPos);
        }
        /// <summary>
        /// This Moves your Champ, this should NEVER be called from outside the Behavior Tree
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void Walk(float x, float y)
        {
            Vector3 walkPos = new Vector3(x, y, 1);
            ZetaDia.Actors.Me.Movement.MoveActor(walkPos);
        }


 


    }
}