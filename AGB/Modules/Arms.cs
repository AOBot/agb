﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AGB.Managerial;
using AGB.Modules.FrontalLobe_;
using Zeta;
using Zeta.Common;
using Zeta.Internals.Actors;
using Zeta.TreeSharp;
using Action = Zeta.TreeSharp.Action;

namespace AGB.Modules
{
    public class Arms : Module
    {
        public Arms()
        {
            Name = "Arms";
            Author = "none";
            Version = "0.1.0";
        }     
   
        public bool UseSkillSelf(SNOPower power)
        {
            return ZetaDia.Me.UsePower(power,new Vector3(), ZetaDia.Me.WorldDynamicId);
        }


    }
}
