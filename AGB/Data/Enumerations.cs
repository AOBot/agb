﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AGB
{
    /// <summary>
    /// Quest enumeration -- thanks Kane49 the full nerd from Germany
    /// </summary>
    public enum Quests
    {
        A1_A_Shattered_Crown = 57331,
        A1_Reign_of_the_Black_King = 72061,
        A1_The_Legacy_of_Cain = 72095,
        A1_Trailing_the_Coven = 72546,
        A1_The_Broken_Blade = 72738,
        A1_The_Imprisoned_Angel = 72801,
        A1_The_Doom_in_Wortham = 73236,
        A1_The_Fallen_Star = 87700,
        A1_Sword_of_the_Stranger = 117779,
        A1_Return_to_New_Tristram = 136656,

        A2_Shadows_in_the_Desert = 80322,
        A2_Road_to_Alcarnus = 74128,
        A2_City_of_Blood = 74128,
        A2_A_Royal_Audience = 57331,
        A2_Unexpected_Allies = 78264,
        A2_Betrayer_of_the_Horadrim = 78266,
        A2_Blood_and_Sand = 57335,
        A2_The_Black_Soulstone = 57337,
        A2_The_Scouring_of_Caldeum = 121792,
        A2_Lord_of_Lies = 57339,

        A3_The_Siege_of_Bastions_keep = 93595,
        A3_Turning_the_Tide = 93684,
        A3_The_Breached_Keep = 93697,
        A3_Tremors_in_the_Stone = 203595,
        A3_Machines_of_War = 101756,
        A3_Siegebreaker = 101750,
        A3_Heart_of_Sin = 101758,

        A4_Fall_of_the_High_Heavens = 112498,
        A4_The_Light_of_Hope = 113910,
        A4_Beneath_the_Spire = 114795,
        A4_Prime_Evil = 114901,

        Sub_Quest_1 = 0,
        Sub_Quest_2 = 1,
        Sub_Quest_3 = 2

    }
}
