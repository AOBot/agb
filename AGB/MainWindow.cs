﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Threading;
using AGB.Managerial;
using D3.Pathing;
using Demonbuddy;
using PSLib;
using Zeta;
using Zeta.Common;
using Zeta.CommonBot;
using Zeta.Internals.Actors;

namespace AGBLoader
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BotManager.Stop();
        }


        private void trackBar1_Scroll(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }


        private void trackBar2_ValueChanged(object sender, EventArgs e)
        {
            GameDelay.Text = "" + trackBar2.Value;
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            TicksPerSecond.Text = "" + trackBar1.Value;
        }

        private void SaveParamsButton_Click(object sender, EventArgs e)
        {
            var settings = AGB.Properties.Settings.Default;
            settings.GameDelay = trackBar2.Value;
            settings.TPS = trackBar1.Value;
            settings.KillswitchCount = int.Parse(textBox1.Text);
            settings.Killswitch = checkBox1.Checked;
            settings.WatchDogDelay = int.Parse(watchdogDelay.Text)*1000;
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            var settings = AGB.Properties.Settings.Default;
            trackBar2.Value = settings.GameDelay;
            trackBar1.Value = settings.TPS;
            textBox1.Text = settings.KillswitchCount + "";
            checkBox1.Checked = settings.Killswitch;
            watchdogDelay.Text = ""+settings.WatchDogDelay/1000;
        }
    }
}
