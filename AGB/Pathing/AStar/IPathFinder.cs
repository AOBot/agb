using System.Collections.Generic;
using System.Drawing;

namespace D3.Pathing
{
    internal interface IPathFinder
    {
        #region Properties

        bool Stopped { get; }

        heurs Formula { get; set; }

        bool Diagonals { get; set; }

        bool HeavyDiagonals { get; set; }

        int HeuristicEstimate { get; set; }

        bool PunishChangeDirection { get; set; }

        bool TieBreaker { get; set; }

        int SearchLimit { get; set; }

        double CompletedTime { get; set; }

        bool DebugProgress { get; set; }

        bool DebugFoundPath { get; set; }

        #endregion

        #region Methods

        void FindPathStop();
        List<SpeedyAStarNode> FindPath(Point start, Point end);

        #endregion
    }
}