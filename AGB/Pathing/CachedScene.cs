﻿using System;
using System.Drawing;
using Zeta.Common;
using Zeta.Internals;
using Zeta.Internals.SNO;

namespace D3.Pathing
{
    public class CachedScene : IDisposable
    {

        private const int DB_Retarded_Offset = 88;
        public Scene Scene;

        public CachedScene(Scene scene)
        {
            Scene = scene;
            
            if (scene.IsValid == false)
            {
                throw new ArgumentException("Scene is not valid (lacking navmesh!)");
            }

foreach (var cell in scene.SceneInfo.NavZone.NavCells)
            {
                //Logging.Write(cell.ToString());
            }         

            

            Scene.NavMesh nm = scene.Mesh;

            X = scene.Mesh.Position.X;
            Y = scene.Mesh.Position.Y;

            //DumpNavCells(scene);

            SizeX = scene.Mesh.SquareCountX;
            SizeY = scene.Mesh.SquareCountY;

            SceneId = scene.SceneGuid;

            int myOffset;

            /*if (SizeY == 96) 
            myOffset = DB_Retarded_Offset / (96 / SizeY);
            else myOffset = SizeY;*/
            myOffset = 89;


            Collisions = new NavMeshSquare[SizeX, SizeY];

            Logging.Write("Scene: " + scene.Name+" "+    SizeX+"*"+SizeY);
          

            NavMeshSquare[] tmpGrid;

            if (scene.SceneInfo.NavMeshDef == null)
            {
                Logging.Write("NavMeshDef = null");
                throw new Exception("Invalid NavMesh Grid");
            }

            try {
                tmpGrid = scene.SceneInfo.NavMeshDef.NavMeshGrid;
            } catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            Logging.Write("" + 5);
            /*for (int i = 0; i < tmpGrid.Length; i++ )
            {
                int x = i%SizeY;
                int y = i/SizeY;

                
                //if (x < myOffset)
                //    if((x + (SizeX - myOffset) < SizeX)) Collisions[x + (SizeX - myOffset), y] = tmpGrid[i];
                //else
                //         Collisions[x - myOffset, y] = tmpGrid[i];

                Collisions[x, y] = tmpGrid[i];

            }*/


            

            /*    for (int x = 0; x < SizeX; x++)
                {
                    for (int y = 0; y < SizeY; y++)
                    {
                        
                        if (x > DB_Retarded_Offset)
                        {
                            Collisions[x - myOffset, y] = tmpGrid[y * SizeY + x];

                        }
                        else
                        {
                            if ((x + (SizeX - myOffset) < SizeX))
                                Collisions[x + (SizeX - myOffset), y] = tmpGrid[y * SizeY + x];
                        }

                    }
                }
            */
                //pt.Save(SceneId + ".bmp");
            //Multiverse.GetCurrentMap().DrawMap(this.SceneId);
        }

        public void DumpNavCells(Scene s)
        {
            Bitmap pt = new Bitmap(512, 512);

            Graphics g = Graphics.FromImage(pt);
            Brush my_pen2 = new SolidBrush(Color.Green);
            Pen my_pen = new Pen(Color.Black);

                foreach (var navCell in s.SceneInfo.NavZone.NavCells)
                {

                    if (navCell.Flags.HasFlag(NavCellFlags.AllowWalk))
                    {
                        float factor = 0.5f;
                        float miX = (navCell.Min.X);
                        float maX = (navCell.Max.X);
                        float miY = (navCell.Min.Y);
                        float maY = (navCell.Max.Y);

                        g.FillRectangle(my_pen2, miX, miY, maX, maY);
                        g.DrawRectangle(my_pen, miX, miY, maX, maY);
                    }
                }
            

            pt.Save(s.SceneGuid + ".bmp");
        }

        public NavMeshSquare[,] Collisions { get; private set; }
        public float X { get; private set; }
        public float Y { get; private set; }
        public int SizeX { get; set; }
        public int SizeY { get; set; }
        public int SceneId { get; private set; }

        public void Dispose()
        {
            Collisions = null;
        }

    }
}