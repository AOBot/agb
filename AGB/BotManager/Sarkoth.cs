﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using AGB.Managerial;
using Zeta.CommonBot;
using Zeta.TreeSharp;

namespace AGB.Bots
{
    class Sarkoth : IBot
    {
        private Composite Behavior;

        public void Dispose()
        {
            Behavior = null;
        }

        public void Start()
        {
            if (Behavior == null) Behavior = BotManager.CerebralCortex.GenerateSarkothQuestBehavior();
        }

        public void Stop()
        {
            
        }

        public void Pulse()
        {
           
        }

        public string Name
        {
            get { return "Sarkoth"; }
        }

        public bool IsPrimary
        {
            get { return true; }
        }

        public Window ConfigWindow
        {
            get { return null; }
        }

        public Composite Logic
        {
            get { return Behavior; }
        }
    }
}
