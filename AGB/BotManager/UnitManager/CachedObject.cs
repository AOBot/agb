﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zeta.Common;
using Zeta.Internals.Actors;
using Zeta.Internals.SNO;

namespace AGB.Managerial
{
    public abstract class CachedObject : IDisposable
    {

        private int _listIndex = 0;
        private DiaObject DiaUnit = null;
        public int ActorSNO = -1;
        public int ACDGuid = -1;
        public String Name = "";
        public bool CachingFailed = false;
        private float _cachedDistance = -1;
        private long _distanceTick = Environment.TickCount-300;
        public bool Invalid = false;

        public CachedObject()
        {
            //UnitManager.EntityCounter++;
        }

        ~CachedObject()
        {
            //UnitManager.EntityCounter--;
        }

        public bool Interact()
        {
            if (IsValid())
            return DiaUnit.Interact();
            return false;
        }

        public void BaseInit(DiaObject nativeObject)
        {
            DiaUnit = nativeObject;
            ActorSNO = DiaUnit.ActorSNO;
            Name = nativeObject.Name;
            ACDGuid = nativeObject.ACDGuid;
        }

        public float getDistance()
        {
            try
            {
                if (DiaUnit == null) return 5000;
                return DiaUnit.Distance;
            }catch (Exception e)
            {
                return 5000;
            }
            /*try
            {
                if (!IsValid()) return 5000;
                if (_distanceTick + 300 > Environment.TickCount)
                {
                    _distanceTick = Environment.TickCount;
                    float dist = DiaUnit.Distance;
                    if (dist < 0) return 5000;
                    _cachedDistance = dist;
                    }
                return _cachedDistance;
            }
            catch (Exception e)
            {
                return 5000;
            }*/
            return DiaUnit.Distance;
        }

        public DiaObject GetNativeObject()
        {
                return DiaUnit;
        }

        public Vector3 GetPosition()
        {
            if (this.DiaUnit == null)
                return new Vector3(-1, -1, -1);
            return DiaUnit.Position;
                
        }

        public virtual bool IsValid()
        {
            if (DiaUnit == null)
            {
                Invalid = true;
                return false;
            }
            if (DiaUnit.BaseAddress == IntPtr.Zero)
            {
                Invalid = true;
                return false;
            }
            return true;
        }

        public bool InLightOfSight()
        {
            if (!IsValid()) return false;
            try
            {
                return DiaUnit.InLineOfSight;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public void Dispose()
        {
            DiaUnit = null;
            Name = null;
        }

    }
}
