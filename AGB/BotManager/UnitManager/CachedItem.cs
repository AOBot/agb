﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zeta.Internals.Actors;

namespace AGB.Managerial
{
    public class CachedItem : CachedObject
    {

        public ItemQuality Quality;
        public CachedItem(DiaItem diaItem)
        {
            try
            {
                if (diaItem == null)
                {
                    CachingFailed = true;
                    return;
                }
                Quality = diaItem.CommonData.ItemQualityLevel;
                base.BaseInit(diaItem);
            }
            catch (Exception e)
            {
                CachingFailed = true;
                return;
            }
        }

        public override bool IsValid()
        {
            if (GetNativeObject() == null) return false;
            return (GetNativeObject().ACDGuid != -1);
        }
    }
}
