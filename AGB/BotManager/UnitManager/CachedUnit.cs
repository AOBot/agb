﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zeta.Common;
using Zeta.Internals.Actors;
using Zeta.Internals.SNO;

namespace AGB.Managerial
{
    public class CachedUnit : CachedObject
    {

        public int TeamId = -1;
        public bool Hostile = false;
        public MonsterAffixEntry[] MonsterAffixes = { };

        public CachedUnit(DiaUnit dunit)
        {
            try{

            if (dunit == null)
            {
                CachingFailed = true;
                return;
            }
            
            
            var teamId = dunit.CommonData.GetAttribute<int>(ActorAttributeType.TeamID);
            Hostile = BotManager.UnitManager.IsTeamHostile(teamId);
            if (Hostile) MonsterAffixes = dunit.CommonData.MonsterAffixEntries.ToArray();
            base.BaseInit(dunit);

            } catch (Exception e)
                {
                    CachingFailed = true;
                   return;
                }
        }
    }
}
