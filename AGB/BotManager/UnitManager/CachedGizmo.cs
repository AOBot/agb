﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zeta.Common;
using Zeta.Internals.Actors;
using Zeta.Internals.SNO;

namespace AGB.Managerial
{
    public class CachedGizmo : CachedObject
    {

        public CachedGizmo(DiaGizmo diaGizmo)
        {
            try
            {
                if (diaGizmo == null)
                {
                    CachingFailed = true;
                    return;
                }
                base.BaseInit(diaGizmo);
            }
            catch (Exception e)
            {
                CachingFailed = true;
                return;
            }
        }
    }
}
