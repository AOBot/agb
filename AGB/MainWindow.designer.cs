﻿namespace AGBLoader
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.SaveParamsButton = new System.Windows.Forms.Button();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.GameDelay = new System.Windows.Forms.TextBox();
            this.TicksPerSecond = new System.Windows.Forms.TextBox();
            this.watchdogDelay = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(165, 207);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Unload";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // SaveParamsButton
            // 
            this.SaveParamsButton.Location = new System.Drawing.Point(12, 207);
            this.SaveParamsButton.Name = "SaveParamsButton";
            this.SaveParamsButton.Size = new System.Drawing.Size(75, 23);
            this.SaveParamsButton.TabIndex = 3;
            this.SaveParamsButton.Text = "Save";
            this.SaveParamsButton.UseVisualStyleBackColor = true;
            this.SaveParamsButton.Click += new System.EventHandler(this.SaveParamsButton_Click);
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(12, 78);
            this.trackBar1.Maximum = 40;
            this.trackBar1.Minimum = 20;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(146, 45);
            this.trackBar1.TabIndex = 4;
            this.trackBar1.TickFrequency = 5;
            this.trackBar1.Value = 20;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            this.trackBar1.ValueChanged += new System.EventHandler(this.trackBar1_ValueChanged);
            // 
            // trackBar2
            // 
            this.trackBar2.LargeChange = 20;
            this.trackBar2.Location = new System.Drawing.Point(12, 22);
            this.trackBar2.Maximum = 3000;
            this.trackBar2.Minimum = 300;
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(158, 45);
            this.trackBar2.SmallChange = 20;
            this.trackBar2.TabIndex = 5;
            this.trackBar2.TickFrequency = 100;
            this.trackBar2.Value = 1000;
            this.trackBar2.ValueChanged += new System.EventHandler(this.trackBar2_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Create Game Delay";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Bot Ticks per Second";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // GameDelay
            // 
            this.GameDelay.Enabled = false;
            this.GameDelay.Location = new System.Drawing.Point(178, 21);
            this.GameDelay.Name = "GameDelay";
            this.GameDelay.Size = new System.Drawing.Size(48, 20);
            this.GameDelay.TabIndex = 9;
            this.GameDelay.Text = "1000";
            // 
            // TicksPerSecond
            // 
            this.TicksPerSecond.Enabled = false;
            this.TicksPerSecond.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.TicksPerSecond.Location = new System.Drawing.Point(178, 78);
            this.TicksPerSecond.Name = "TicksPerSecond";
            this.TicksPerSecond.Size = new System.Drawing.Size(48, 20);
            this.TicksPerSecond.TabIndex = 10;
            this.TicksPerSecond.Text = "20";
            // 
            // watchdogDelay
            // 
            this.watchdogDelay.Location = new System.Drawing.Point(178, 129);
            this.watchdogDelay.Name = "watchdogDelay";
            this.watchdogDelay.Size = new System.Drawing.Size(48, 20);
            this.watchdogDelay.TabIndex = 11;
            this.watchdogDelay.Text = "60";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Watchdog Delay in Seconds";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(178, 172);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(48, 20);
            this.textBox1.TabIndex = 13;
            this.textBox1.Text = "200";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(75, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Exit after X Deaths";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(45, 175);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 15;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(252, 242);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.watchdogDelay);
            this.Controls.Add(this.TicksPerSecond);
            this.Controls.Add(this.GameDelay);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.trackBar2);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.SaveParamsButton);
            this.Controls.Add(this.button2);
            this.Name = "MainWindow";
            this.Text = "AGB-0.3.3";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button SaveParamsButton;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox GameDelay;
        private System.Windows.Forms.TextBox TicksPerSecond;
        private System.Windows.Forms.TextBox watchdogDelay;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}

