﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Zeta.CommonBot;

namespace AGB.IDemonbuddy
{
    /// <summary>
    /// Interaction logic for AGBSettingsWindow.xaml
    /// </summary>
    public partial class AGBSettingsWindow : Window
    {

        public AGBSettingsWindow()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            var settings = AGB.Properties.Settings.Default;
            settings.GameDelay = (int)slider1.Value;
            settings.TPS = (int)slider2.Value;
            BotMain.TicksPerSecond = settings.TPS;
            settings.KillswitchCount = int.Parse(KillswitchCount.Text);
            settings.Killswitch = Killswitch.IsChecked ?? false;
            settings.WatchDogDelay = int.Parse(WatchdogTimer.Text) * 1000;
            settings.LoadOnStartup = checkBox1.IsChecked ?? false;
            settings.FerretLoot = checkBox2.IsChecked ?? false;
            settings.SaveExitGame = checkBox3.IsChecked ?? false;
            settings.NoIdentify = checkBox4.IsChecked ?? false;
            settings.Towrun = radioButton2.IsChecked ?? false;
            AGB.Properties.Settings.Default.Save();

            ZetaWrap.OOGTimeout = AGB.Properties.Settings.Default.SaveExitGame ? 1000 : 100;

            this.Hide();
        }

        private void slider1_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            GameDelay.Text = ""+(int)slider1.Value;
        }

        private void slider2_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            TPS.Text = "" + (int) slider2.Value;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var settings = AGB.Properties.Settings.Default;
            BotMain.TicksPerSecond = settings.TPS;
            slider1.Value = settings.GameDelay;
            slider2.Value = settings.TPS;
            KillswitchCount.Text = "" + settings.KillswitchCount;
            Killswitch.IsChecked = settings.Killswitch;
            WatchdogTimer.Text = ""+settings.WatchDogDelay/1000;
            checkBox1.IsChecked = settings.LoadOnStartup;
            checkBox2.IsChecked = settings.FerretLoot;
            checkBox3.IsChecked = settings.SaveExitGame;
            checkBox4.IsChecked = settings.NoIdentify;
            radioButton2.IsChecked = settings.Towrun;
        }
    }
}
