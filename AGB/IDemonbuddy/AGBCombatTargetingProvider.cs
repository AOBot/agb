﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AGB.Managerial;
using Zeta.CommonBot;
using Zeta.Internals.Actors;

namespace AGB.IDemonbuddy
{
    class AGBCombatTargetingProvider : ITargetingProvider
    {
        public List<DiaObject> GetObjectsByWeight()
        {
            var List = new List<DiaObject>();
            var Queue = BotManager.UnitManager.GetHostiles();
            while (Queue.Count > 0) List.Add(Queue.DequeueValue().GetNativeObject());
            return List;
        }
    }
}
