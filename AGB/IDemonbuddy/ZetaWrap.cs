﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zeta;
using Zeta.Common;
using Zeta.CommonBot;
using Zeta.CommonBot.Settings;
using Zeta.Internals;
using Zeta.Internals.Actors;

namespace AGB.IDemonbuddy
{
    public static class  ZetaWrap
    {

        private static long _oogTick = Environment.TickCount;
        private static long _igTick = Environment.TickCount;
        private static bool _ig = true;
        private const long IGTimeout = 250;
        public static long OOGTimeout = 100;
        private static int _iv = 60;

        static ZetaWrap()
        {
            OOGTimeout = AGB.Properties.Settings.Default.SaveExitGame ? 1000 : 100;
        }

        public static int InventorySlots
        {
            get { return _iv; }
            set { _iv = value; }
        }

        public static bool IsDead()
        {
            try
            {
                if (ZetaDia.Me == null) return false;
                bool dead = ZetaDia.Me.IsDead;
                return dead;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool IsInTown()
        {
            try
            {
                if (ZetaDia.Me == null) return false;
                bool town = ZetaDia.Me.IsInTown;
                return town;
            }
            catch (Exception e)
            {
                return false;
            }           
        }

        public static bool OutOfGame()
        {
            try
            {
                if (_oogTick + OOGTimeout > Environment.TickCount) return false;

                _oogTick = Environment.TickCount;

                if (AGB.Properties.Settings.Default.SaveExitGame)
                {
                    if (UIElement.IsValidElement(0xC5B12B9C86802B92))
                    {
                        UIElement gui = UIElement.FromHash(0xC5B12B9C86802B92);
                        if (gui == null)
                        {
                            return false;
                        }
                        if (gui.IsVisible)
                        {
                            return true;
                        }
                    }
                    return false;
                }
                else
                {

                    bool oog = !ZetaWrap.IsInGame() && !ZetaDia.IsLoadingWorld;
                    return oog;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool NeedsRepair()
        {
            try {
            if (ZetaDia.Me.Inventory.Equipped.Min<ACDItem>(item => item.DurabilityPercent) <= CharacterSettings.Instance.RepairWhenDurabilityBelow)
            {
                //Logging.Write("perc: " + ZetaDia.Me.Inventory.Equipped.Min<ACDItem>(item => item.DurabilityPercent) + " < " + CharacterSettings.Instance.RepairWhenDurabilityBelow);
                return true;
            }
                

                return false;
            } catch (Exception e)
            {
                return false;
            }

        }

        public static bool IsInGame()
        {
            try
            {
                if (_igTick + IGTimeout > Environment.TickCount) return _ig;
                _igTick = Environment.TickCount;
                if (ZetaDia.Me == null)
                {
                    _ig = false;
                    return _ig;
                }

                _ig = ZetaDia.IsInGame;
                if (_ig)
                {
                    BotMain.TicksPerSecond = AGB.Properties.Settings.Default.TPS;
                }
                return _ig;
            }
            catch (Exception e)
            {
                return _ig;
            }           
        }

        public static bool ForceVendoring = false;

        public static bool IsLoadingWorld()
        {
            try
            {
                bool inGame = ZetaDia.IsLoadingWorld;
                return inGame;
            }
            catch (Exception e)
            {
                return true;
            }      
        }
    }
}
