﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using AGB.Managerial;
using Zeta.CommonBot;
using Zeta.TreeSharp;

namespace AGB.IDemonbuddy
{
    class AGBBot : IBot
    {
        public void Dispose()
        {
            
        }

        public void Start()
        {
            
        }

        public void Stop()
        {
           
        }

        public void Pulse()
        {
            
        }

        public string Name
        {
            get { return "AGB"; }
        }

        public bool IsPrimary
        {
            get { return true; }
        }

        public Window ConfigWindow
        {
            get { return null; }
        }

        public Composite Logic
        {
            get { return BotManager.CerebralCortex.GenerateSarkothQuestBehavior(); }
        }
    }
}
