﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Zeta;
using Zeta.Common;
using Zeta.Common.Plugins;
using Zeta.CommonBot;
using Zeta.Internals.Actors;
using Zeta.TreeSharp;
using Action = System.Action;

namespace AGB.IDemonbuddy
{
    class ThiolesIdentify : IPlugin
    {
        public bool HasToIdentify(ACDItem item)
        {
            //Important to get semi accurate data
            //item.ParseItemTable();
            if (item.InternalName.ToLower().Contains("bow")) return true;
            if (item.Name != "whatiwanttobeidentified") return false;
            /*Insert your logic for what items should be identified here*/
            return true;
        }

        private void IdentifyItem(object ret)
        {
            ZetaDia.Me.Inventory.IdentifyItem(ZetaDia.Me.Inventory.Backpack.First(HasToIdentify).DynamicId);
        }

        private void HandleBotStart(IBot bot)
        {
            foreach (var hook in TreeHooks.Instance.Hooks)
            {
                //Stealing Hook
                if (hook.Key.Contains("VendorRun"))
                {
                    //Traversing the Behavior Tree using magic
                    var dec = hook.Value[0] as Decorator;
                    var prio = dec.Children[0] as PrioritySelector;

                    //Building the new Identify Mechanic
                    var children = new Composite[4];

                    //Decorator to check if any items that we need to identify are left
                    CanRunDecoratorDelegate itemsLeftToIdentify = context => ZetaDia.Me.Inventory.Backpack.Any(HasToIdentify);

                    //Sleep for a bit (1000ms)
                    children[0] = new Sleep(1000);
                    //Start Identify
                    children[1] = new Zeta.TreeSharp.Action(new ActionSucceedDelegate(IdentifyItem));
                    //Sleep for Identify Duration
                    children[2] = new Sleep(0x1388);
                    //Return
                    children[3] = new Zeta.TreeSharp.Action(ctx => RunStatus.Success);

                    prio.Children[2] = new Decorator(itemsLeftToIdentify, new PrioritySelector(children));
                }
            }
        }

        public bool Equals(IPlugin other)
        {
            return (Name == other.Name);
        }

        public void OnPulse()
        {
            
        }

        public void OnInitialize()
        {
            
        }

        public void OnShutdown()
        {
            
        }

        public void OnEnabled()
        {
            Logging.Write("ThioleIdentify: Adding hooks");
            Zeta.CommonBot.BotMain.OnStart += HandleBotStart;
        }

        public void OnDisabled()
        {
            Logging.Write("ThioleIdentify: Removing hooks");
            Zeta.CommonBot.BotMain.OnStart -= HandleBotStart;
        }

        public string Author
        {
            get { return "Kane49"; }
        }

        public Version Version
        {
            get { return new Version(0,1); }
        }

        public string Name
        {
            get { return "Thiole Identify"; }
        }

        public string Description
        {
            get { return "none"; }
        }

        public Window DisplayWindow
        {
            get { return null; }
        }
    }
}
