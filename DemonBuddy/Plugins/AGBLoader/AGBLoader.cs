﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using Zeta;
using Zeta.Common;
using Zeta.Common.Plugins;
using Zeta.CommonBot;
using Zeta.CommonBot.Logic;
using Zeta.CommonBot.Profile;
using Zeta.Internals;
using Zeta.Internals.Actors;
using Zeta.Internals.Service;
using Application = System.Windows.Forms.Application;

namespace AGBLoader
{
    public class AGBLoader : IPlugin
    {
	
        #region Iplugin
        public bool Equals(IPlugin other)
        {
            // we should probably be comparing versions too
            return other.Name == Name;
        }

        public string Author
        {
            get { return "Kane49"; }
        }

        public string Description
        {
            get { return "Loads the AGB DLL"; }
        }

        public string Name
        {
            get { return "AGB.Loader"; }
        }

        public Version Version
        {
            get { return new Version(1, 0); }
        }

        public Window DisplayWindow { get { 
			return (Window) _currentAssembly.GetProperty("aWindow", BindingFlags.Public | BindingFlags.Static).GetValue(null,null);
		} 
		}

        /// <summary> Executes the shutdown action. This is called when the bot is shutting down. (Not when Stop() is called) </summary>
        public void OnShutdown()
        {
        }

        /// <summary> Executes the enabled action. This is called when the user has enabled this specific plugin via the GUI. </summary>
        public void OnEnabled()
        {
			Logging.Write("Enabled AGB");
			if (_currentAssembly == null) {
            			var asm = File.ReadAllBytes("plugins/AGBLoader/AGB.dll");
            			_currentAssembly = Assembly.Load(asm).GetType("_Demonbuddy");
			}
			_currentAssembly.GetMethod("OnLoad").Invoke(null, null);
        }

        /// <summary> Executes the disabled action. This is called whent he user has disabled this specific plugin via the GUI. </summary>
        public void OnDisabled()
        {
			_currentAssembly.GetMethod("Unload").Invoke(null, null);
			_currentAssembly = null;
        }

        #endregion

        private Type _currentAssembly;

        public void OnInitialize()
        {
            var asm = File.ReadAllBytes("plugins/AGBLoader/AGB.dll");
            _currentAssembly = Assembly.Load(asm).GetType("_Demonbuddy");
	    _currentAssembly.GetMethod("OnLoadCond").Invoke(null, null);
        }
        
        public void OnPulse()
        {

        }
    }
}
