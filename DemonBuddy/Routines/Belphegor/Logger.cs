﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using Zeta.Common;

namespace Belphegor
{
    public static class Logger
    {
        public static void Write(string message)
        {
            Write(Colors.Green, message);
        }

        public static void Write(string message, params object[] args)
        {
            Write(Colors.Green, message, args);
        }

        public static void Write(Color clr, string message, params object[] args)
        {
            Logging.Write(clr, "[Belphegor] " + message, args);
        }

        public static void WriteVerbose(string message)
        {
            WriteVerbose(Colors.Green, message);
        }

        public static void WriteVerbose(string message, params object[] args)
        {
            WriteVerbose(Colors.Green, message, args);
        }

        public static void WriteVerbose(Color clr, string message, params object[] args)
        {
            Logging.WriteVerbose(clr, "[Belphegor] " + message, args);
        }
    }
}
