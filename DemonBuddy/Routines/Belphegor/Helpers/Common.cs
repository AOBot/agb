﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zeta;
using Zeta.Common.Helpers;
using Zeta.Internals.Actors;
using Zeta.TreeSharp;
using Action = Zeta.TreeSharp.Action;
using Zeta.Common;

using Belphegor.Settings;

namespace Belphegor.Helpers
{
    public static class Common
    {
        public static Composite CreateWaitWhileIncapacitated()
        {
            return
                new Decorator(ret =>
                    ZetaDia.Me.IsFeared ||
                    ZetaDia.Me.IsStunned ||
                    ZetaDia.Me.IsFrozen ||
                    ZetaDia.Me.IsBlind ||
                    ZetaDia.Me.IsRooted,

                    new Action(ret => RunStatus.Success)
                );
        }

        public static Composite CreateWaitForAttack()
        {
            return
                new Decorator(ret => ZetaDia.Me.CommonData.AnimationState == AnimationState.Attacking,
                    new Action(ret => RunStatus.Success)
                );

        }

        private static WaitTimer _potionCooldownTimer = WaitTimer.ThirtySeconds;
        public static Composite CreateUsePotion()
        {
            return
                new Decorator(ret => ZetaDia.Me.HitpointsCurrentPct <= BelphegorSettings.Instance.HealthPotionPct && _potionCooldownTimer.IsFinished,
                    new PrioritySelector(ctx => ZetaDia.Me.Inventory.Backpack.Where(i => i.IsPotion && i.RequiredLevel <= ZetaDia.Me.Level).OrderByDescending(p => p.HitpointsGranted).FirstOrDefault(),

                        new Decorator(ctx => ctx != null,
                            new Sequence(
                                new Action(ctx => ZetaDia.Me.Inventory.UseItem(((ACDItem)ctx).DynamicId)),
                                new Action(ctx => _potionCooldownTimer.Reset()),
                                new Action(ctx => Logger.Write("Potion set to use below {0}%, my health is currently {1}%, using {2}", 100 * BelphegorSettings.Instance.HealthPotionPct, Math.Round(100 * ZetaDia.Me.HitpointsCurrentPct), ((ACDItem)ctx).Name))
                                )
                            )
                        )
                    );
        }
    }
}
