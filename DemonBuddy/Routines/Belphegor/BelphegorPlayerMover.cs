﻿using Zeta;
using Zeta.Common;
using Zeta.Navigation;

namespace Belphegor
{
    public class BelphegorPlayerMover : IPlayerMover
    {
        #region Implementation of IPlayerMover

        public void MoveTowards(Vector3 to)
        {
            ZetaDia.Me.Movement.MoveActor(to);
        }

        public void MoveStop()
        {
            ZetaDia.Me.Movement.MoveActor(ZetaDia.Me.Position);
        }

        #endregion
    }
}
