﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Zeta;
using Zeta.CommonBot;
using Zeta.Internals.Actors;
using Zeta.TreeSharp;
using Action = Zeta.TreeSharp.Action;

namespace AGB.IDemonbuddy
{
    class AGBCombat : CombatRoutine
    {
        public override void Dispose()
        {
            
        }


        public override void Initialize()
        {
            
        }

        public override string Name
        {
            get { return "AGB - Nullcombat"; }
        }

        public override Window ConfigWindow
        {
            get { return null; }
        }

        public override ActorClass Class
        {
            get
            {
                if (!ZetaDia.IsInGame || ZetaDia.IsLoadingWorld)
                {
                    return ActorClass.Invalid;
                }

                return ZetaDia.Me.ActorClass;
            }
        }

        public override Composite Buff
        {
            get
            {
                return new Action(delegate(object context)
                                      {
                                          /*doNothing*/
                                      });
            }
        }

        public override Composite Combat
        {
            get
            {
                return new Action(delegate(object context)
                                      {
                                          /*doNothing*/
                                      });
            }
        }

        public override float DestroyObjectDistance
        {
            get { return 0; }
        }

        public override SNOPower DestroyObjectPower
        {
            get { return 0; }
        }
    }
}
