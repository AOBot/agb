﻿using System;
using System.Drawing;
using Zeta.Common;
using Zeta.Internals;
using Zeta.Internals.SNO;

namespace D3.Pathing
{
    public class CachedScene
    {

        private const int DB_Retarded_Offset = 88;


        public CachedScene(Scene scene)
        {

            
            if (scene.IsValid == false)
            {
                throw new ArgumentException("Scene is not valid (lacking navmesh!)");
            }

foreach (var cell in scene.SceneInfo.NavZone.NavCells)
            {
                //Logging.Write(cell.ToString());
            }         

            Scene.NavMesh nm = scene.Mesh;

            X = scene.Mesh.Position.X;
            Y = scene.Mesh.Position.Y;

            SizeX = scene.Mesh.SquareCountX;
            SizeY = scene.Mesh.SquareCountY;

            SceneId = scene.SceneGuid;

            int myOffset;

            if (SizeY == 96) myOffset = DB_Retarded_Offset / (96 / SizeY);
            else myOffset = SizeY;

            Collisions = new NavMeshSquare[SizeX, SizeY];

            //Logging.Write("Scene: " + scene.Name+" "+
            //    SizeX+"*"+SizeY);
          

            NavMeshSquare[] tmpGrid;

            if (scene.SceneInfo.NavMeshDef == null)
            {
                Logging.Write("NavMeshDef = null");
                throw new Exception("Invalid NavMesh Grid");
            }

            try {
                tmpGrid = scene.SceneInfo.NavMeshDef.NavMeshGrid;
            } catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            for (int i = 0; i < tmpGrid.Length; i++ )
            {
                int x = i%SizeY;
                int y = i/SizeY;

                
                if (x < myOffset)
                    if((x + (SizeX - myOffset) < SizeX)) Collisions[x + (SizeX - myOffset), y] = tmpGrid[i];
                else
                    Collisions[x - myOffset, y] = tmpGrid[i];
                

                //Collisions[x, y] = tmpGrid[i];


                /*if (Collisions[x,y].Flags.HasFlag(Zeta.Internals.SNO.NavCellFlags.AllowWalk))
                {
                    //g.DrawRectangle(my_pen, (float)x, (float)y, (float)2, (float)2);
                    if (x < myOffset)
                        g.DrawRectangle(my_pen2, (float)(x + (SizeX - myOffset)), (float)y, (float)2, (float)2);
                    else
                        g.DrawRectangle(my_pen2, (float)(x - myOffset), (float)y, (float)2, (float)2);
                       
                }*/
            }

            //pt.Save(scene.Name+".bmp");

                /*for (int x = 0; x < SizeX; x++)
                {
                    for (int y = 0; y < SizeY; y++)
                    {

                    
                        if (x > DB_Retarded_Offset)
                        {
                            Collisions[x - DB_Retarded_Offset, y] = tmpGrid[y * SizeY + x];

                        }
                        else
                        {
                            if (x + (SizeX - DB_Retarded_Offset) < SizeX) 
                            Collisions[x+(SizeX - DB_Retarded_Offset), y] = tmpGrid[y * SizeY + x];
                        }
                    

                            if (tmpGrid[y*SizeY + x].Flags.HasFlag(Zeta.Internals.SNO.NavCellFlags.AllowWalk))
                            {
                                if (x > DB_Retarded_Offset)
                                {
                                    g.DrawRectangle(my_pen, (float)x - DB_Retarded_Offset, (float)y, (float)2, (float)2);
                                }
                                else
                                {
                                    if (x + (SizeX - DB_Retarded_Offset) < SizeX) 
                                    g.DrawRectangle(my_pen, (float)x+(SizeX-DB_Retarded_Offset) , (float)y, (float)2, (float)2);
                                }
                            }
                           


                    }
                }*/

                //pt.Save(SceneId + ".bmp");
            
        }

        public NavMeshSquare[,] Collisions { get; private set; }
        public float X { get; private set; }
        public float Y { get; private set; }
        public int SizeX { get; set; }
        public int SizeY { get; set; }
        public int SceneId { get; private set; }
    }
}