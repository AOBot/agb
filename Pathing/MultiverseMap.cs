﻿using System;
using System.Collections.Generic;
using D3;
using Zeta.Common;


namespace D3.Pathing
{
    public static class Multiverse
    {
        private static bool _drawMap;

        static Multiverse()
        {
            MultiverseMap = new Dictionary<int, Map>();

        }

        private static Dictionary<int, Map> MultiverseMap { get; set; }

        public static void ToogleMapDraw()
        {
            if (_drawMap) _drawMap = false;
            else _drawMap = true;
        }


        public static bool containsWid()
        {
            return MultiverseMap.ContainsKey(Zeta.ZetaDia.CurrentWorldId);
        }

        public static void init()
        {
            //Does Nothing but makes sure me init statics
        }

        public static void Clear()
        {
            MultiverseMap.Clear();
        }

        public static Map GetCurrentMap()
        {
            if (!MultiverseMap.ContainsKey(Zeta.ZetaDia.CurrentWorldId))
                MultiverseMap.Add(Zeta.ZetaDia.CurrentWorldId,new Map(Zeta.ZetaDia.CurrentWorldId));
            return MultiverseMap[Zeta.ZetaDia.CurrentWorldId];
        }
    }
}