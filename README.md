# What is this #
A botting framework for D3

# What is different #
Behavior trees There are a few frameworks popping up now, the most important aspect that makes AGB different is that it is based on behavior trees. Every action done is a task. Each task is small it is simple to change course whenever is needed and assign new tasks to handle the new situation. Tasks are added asynchronously by many modules at once, based on events. AGB will take in data from the game, fire events letting the Modules know important information in the game allowing them to add tasks.

**Current modules in development:**

* Mover
* MagicFinder
* Killer
* Statistics
* Chicken (potion management/retreating)
* Stfu (closes out of cut scenes/npc dialogue)
* TownManager

**Example**: MagicFinder is pathing from one area to another using Mover, taking every warp it finds in search of elite mobs/chests. If MagicFinder come across a horrifying elite pack, Chicken will run away using Mover to a safe spot at a previously cleared breadcrumb. If MagicFinder finds a nice and easy mob, it will use Killer to find the best attack strategies/patterns to use in fighting it.

**No configuration required** The last thing unique to AGB is that there is no configuration required. You could have configurations that you prefer while botting and set them manually, or allow each module to best determine what course of action needs to be taken. Every module is required to be able to run without configuration and decide which places to go to, what skills to use/how to use them, and what to do with items. We know a Witch Doctor might not be good at some quests/areas, so we won't send them there. Having to set these things is annoying and for a majority of simple botters a hassle to understand/configure.

This system ideal for robust botting, like traveling throughout an act killing elite mobs... it's a dream bot and going to be a fucking fun project for us all.